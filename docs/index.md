# زلزال مرعش

![](assets/banner.png)

للمساهمة والتطوع في أعمال الإغاثة والإنقاذ جمعت قاعدة البيانات هذه لتكون دليلا يسهل الوصول للمعلومة في هذا الوقت الحرج.

## الدعم المادي

### جمعيات تركية ذات سمعة
 * [جمعية الحقوق والحريات İHH](https://ihh.org.tr/ar) جمعية أهلية لها نشاطات في البلاد الاسلامية وتركيا
 * [آفاد AFAD](https://www.afad.gov.tr/depremkampanyasi2) الهيئة الحكومية الرسمية لإدارة الآفات والكوارث وتنسيق الإغاثة فيها
 * [الهلال الأحمر](https://www.kizilay.org.tr/)

### جمعيات عربية

* [جمعية قضاء الحوائج](./جمعية_قضاء_الحوائج)
* [مؤسسة زيد بن ثابت](./مؤسسة_زيد_بن_ثابت)

## التطوع

### التبرع بالدم

من أكثر الاحتياجات هو التبرع للدم للمساهمة بانقاذ الجراحى. 

 * [مراكز الهلال الأحمر للتبرع بالدم](https://www.kanver.org/KanHizmetleri/KanBagisiNoktalari)

من مراكزها الرئيسية مركز تشابا Çapa في الفاتح عند محطة ترامواي Çapa في شارع يوسف باشا.

### زيارة المناطق

أغلب التصريحات تشير إلى أن الوصول إلى المناطق المنكوبة صعب ويحتاج لتصاريح وبعضها يشير إلى منع السيارات المدنية من التوجه لإفساح المجال للهيئات الرسمية.

إضافة إلى أن انقطاع الكهرباء والوقود والغاز فإن المحترفين فقط العاملين في جهات مجهزة منظمة هي من يرجى الفعالية من توجهها.

## المفقودين

### قوائم المفقودين 

* [قوائم المفقودين اتحاد طلبة سوريا](https://www.ssunion.org/earthquake-missings)

## مبادرات مجتمعية

### مجموعات على الانترنت



